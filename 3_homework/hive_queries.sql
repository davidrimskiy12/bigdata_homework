CREATE SCHEMA IF NOT EXISTS test;
#created table from avro
CREATE EXTERNAL TABLE my_avro
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.avro.AvroSerDe'TBLPROPERTIES
('avro.schema.url'='hdfs://localdomain/user/avro/schemas/activity.avsc')
STORED AS INPUTFORMAT
'org.apache.hadoop.hive.ql.io.avro.AvroContainerInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.avro.AvroContainerOutputFormat'
LOCATION
'/user/hdfs/my_shema.avsc';

#csv file in table
CREATE EXTERNAL TABLE IF NOT EXISTS my_csv
(id BIGINT ,Name STRING,City STRING,Address STRING,Lattitude String, Longitude STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION 'hdfs:///user/hdfs/hotels';
#create weathers table
Create external table my_parquet
(lng double, lat double, avg_tmp_f double, avg_tmp_c double, wthr_date String)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
stored as parquet
location "hdfs:///user/hdfs/weather";
#join csv with avro
CREATE TABLE avro_csv_join AS
SELECT * FROM my_csv 
 JOIN myavro 
ON myavro.hotel_id = my_csv.my_id;
#full join
CREATE TABLE full_join AS
SELECT * FROM my_parquet 
JOIN  avro_csv_join 
On round(avro_csv_join.lattitude,2)=round(myparquet.lattitude,2)  and
round(avro_csv_join.longitude,2)=round(myparquet.longitude,2)  ;
#1 work
SELECT  MONTH(myavro.date_time) , MIN(my_parquet.avg_tmp_c) from myavro, my_parquet
group by MONTH(myavro.date_time)
limit 10;
#doesnt work but im trying like that
-- I too try to do like that (and some another’s ways, but it too doesn’t worked ):
-- SELECT MONTH(myavro.date_time) as month, MIN(my_parquet.a1) as temp_min, MAX(my_parquet.a1) as max_temp from myavro, my_parquet
-- where max_temp - min_temp > 40
-- group by month
-- limit 10;
-- set min_temp = (select MIN(my_parquet.avg_tmp_f) from my_parquet);
-- set max_temp = (select MAX(my_parquet.avg_tmp_f) from my_parquet);
-- set visit counts = (select( myavro.srch_adults_cnt +myavro.srch_children_cnt ));
 #2
 SELECT  MONTH(myavro.date_time), max(myavro.srch_adults_cnt) from myavro
group by MONTH(myavro.date_time)
limit 10;
#3
set first_day_t = (select avg_tmp_c, srch_co from full_data);
set last_day_t = (select avg_tmp_c, srch_ci from full_data);
set odds = (last_day_t - first_day_t);
SELECT day(srch_ci) as last , day(srch_ci) as first, avg_tmp_c , date_time, odds from full_data 
where last - first > 7;



