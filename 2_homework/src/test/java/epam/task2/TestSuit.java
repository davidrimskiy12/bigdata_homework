package epam.task2;

import epam.task2.utils.CustomKey;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestSuit {
    private MapDriver<LongWritable, Text, CustomKey, Text> mapDriver;
    private ReduceDriver<CustomKey, Text, Text, IntWritable> reduceDriver;
    private MapReduceDriver<LongWritable, Text, CustomKey, Text, Text, IntWritable> mapReduceDriver;
    @Before
    public void setUp()
    {

        MyMapper mapper = new MyMapper();
        mapDriver = new MapDriver<LongWritable, Text, CustomKey, Text>();
        mapDriver.setMapper(mapper);

        MyReducer reducer = new MyReducer();
        reduceDriver = new ReduceDriver<CustomKey, Text, Text, IntWritable>();
        reduceDriver.setReducer(reducer);

        mapReduceDriver = new MapReduceDriver<LongWritable, Text, CustomKey, Text, Text, IntWritable>();
        mapReduceDriver.setMapper(mapper);
        mapReduceDriver.setReducer(reducer);
    }
    @Test
    public void testMapper() throws IOException
    {
        String value = "18,2015-08-18 12:37:10,2,3,57,342,5021,null,57,0,0,5,2017-09-19,2017-09-20,2,0,1,8243,1,2826088480774\n" +
                "20,2015-09-01 06:09:06,2,3,57,342,5021,null,57,0,0,5,2017-08-01,2017-08-03,2,0,1,8219,1,1898375544837\n" +
                "26,2015-08-21 21:36:18,11,3,205,135,36086,7449.5906,87,0,0,3,2017-09-23,2017-09-24,2,0,1,8217,1,1425929142277\n";
        String [] field = value.split(",");
        mapDriver.withInput(new LongWritable(),new Text("1"));
        mapDriver.withOutput(new CustomKey(field[0], field[2], field[12], field[11], field[19], field[14]), new Text("1"));
        mapDriver.withOutput(new CustomKey(field[20], field[21], field[31], field[30], field[38], field[33]), new Text("1"));
        mapDriver.withOutput(new CustomKey(field[39], field[40], field[50], field[49], field[57], field[52]), new Text("1"));
        mapDriver.runTest();
    }
    @Test
    public void testReducer() throws IOException
    {
        List values = new ArrayList();
        values.add(new IntWritable(1));
        values.add(new IntWritable(1));
        reduceDriver.withInput(new CustomKey("123123213","12:04","fdfd","23","123","3"),values);
        reduceDriver.withOutput(new Text("pam-pam"), new IntWritable(2));
        reduceDriver.runTest();
    }
    @Test
    public void testMapperReducer() throws IOException
    {
        Text value = new Text("18,2015-08-18 12:37:10,2,3,57,342,5021,null,57,0,0,5,2017-09-19,2017-09-20,2,0,1,8243,1,2826088480774\n" +
                "20,2015-09-01 06:09:06,2,3,57,342,5021,null,57,0,0,5,2017-08-01,2017-08-03,2,0,1,8219,1,1898375544837\n" +
                "26,2015-08-21 21:36:18,11,3,205,135,36086,7449.5906,87,0,0,3,2017-09-23,2017-09-24,2,0,1,8217,1,1425929142277\n");
        mapReduceDriver.addInput(new LongWritable(1), value);
        mapReduceDriver.addOutput(new Text("null"), new IntWritable(2));
        mapReduceDriver.addOutput(new Text("1"), new IntWritable(1));
        mapReduceDriver.runTest();
    }

}
