package epam;
import epam.task2.MyMapper;
import epam.task2.MyReducer;
import epam.task2.utils.CustomKey;
import epam.task2.utils.GroupComparator;
import epam.task2.utils.MyComparator;
import epam.task2.utils.MyPartitioner;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class Driver {
    public static void main(String args[]) throws Exception
    {
        if(args.length != 2)
        {
            System.err.println("Inavlid Command!");
            System.err.println("Usage: PamPam <input path> <output path>");
            System.exit(0);
        }

        Job job = new Job();
        job.setJarByClass(Driver.class);
        job.setJobName("PamPam");
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setMapperClass(MyMapper.class);
        job.setReducerClass(MyReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setCombinerKeyGroupingComparatorClass(GroupComparator.class);
        job.setPartitionerClass(MyPartitioner.class);
        job.setGroupingComparatorClass(GroupComparator.class);
        job.setSortComparatorClass(MyComparator.class);
        job.setReducerClass(MyReducer.class);
        job.setMapOutputKeyClass(CustomKey.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.setNumReduceTasks(4);


        System.exit(job.waitForCompletion(true)?0:1);
    }
}
