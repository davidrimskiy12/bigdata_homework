package epam.task2;

import epam.task2.utils.CustomKey;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<LongWritable, Text, CustomKey, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] data = line.split("\n");
        if (!line.isEmpty()) {
            int i = 0;
            String[] values = data[i++].trim().split(",");
            String Id = values[0];
            String dateTime = values[1];
            String srchSi = values[12];
            String channel = values[11];
            String srchAdult = values[14];
            String hotelId = values[19];
            Integer adult = Integer.parseInt(srchAdult);
            if (adult >= 2) {
                context.write(new CustomKey(Id, dateTime, srchSi, channel, hotelId, srchAdult), new Text("1"));
            }
        }
    }
}