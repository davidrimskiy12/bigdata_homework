package epam.task2;

import epam.task2.utils.CustomKey;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MyReducer extends Reducer<CustomKey, Text, Text, IntWritable> {

    protected void reduce(CustomKey key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {
        int count = 0;
        String name = null;
        for (Text value : values) {
            if (!value.toString().matches("[0-9]")) {
                name = value.toString();
            } else if (value.toString().matches("[0-9]")) {
                count += Integer.parseInt(value.toString());
            }
        }
        if (count != 0) context.write(new Text(name), new IntWritable(count));
    }
}
