package epam.task2.utils;


import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

import static java.lang.Integer.MAX_VALUE;

public class MyPartitioner extends Partitioner<CustomKey, Text> {
    @Override
    public int getPartition(CustomKey customKey, Text text, int i) {
        return ((customKey.getHotelId().hashCode()&MAX_VALUE) % i);
    }
}

