package epam.task2.utils;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class GroupComparator extends WritableComparator {
    protected GroupComparator() {
        super(CustomKey.class, true);
    }

    @Override
    public int compare(WritableComparable writable1, WritableComparable writable2) {
        CustomKey firstKey = (CustomKey) writable1;
        CustomKey secondKey = (CustomKey) writable2;
        return firstKey.getHotelId().compareTo(secondKey.getHotelId());
    }
}

