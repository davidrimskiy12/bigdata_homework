package epam.task2.utils;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class MyComparator extends WritableComparator {
    protected MyComparator() {
        super(CustomKey.class, true);
    }
    @Override
    public int compare(WritableComparable writable1, WritableComparable writable2) {
        CustomKey firstKey = (CustomKey) writable1;
        CustomKey secondKey = (CustomKey) writable2;
        if (firstKey.getHotelId().compareTo(secondKey.getHotelId()) == 0) {
            if (firstKey.getSrchSi().compareTo(secondKey.getSrchSi()) != 0) {
                return -firstKey.getSrchSi().compareTo(secondKey.getSrchSi());
            } else if (firstKey.getId().compareTo(secondKey.getId()) != 0) {
                return -firstKey.getId().compareTo(secondKey.getId());
            } else
                return 0;
        }
        return firstKey.getHotelId().compareTo(secondKey.getHotelId());
    }
}

