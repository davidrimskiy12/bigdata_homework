package epam.task2.utils;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class CustomKey implements WritableComparable<CustomKey> {
    private static String  id;
    private static String dateTime;
    private static String channel;
    private static String srchSi;
    private static String srchAdultsCount;
    private static String hotelId;

    private static SimpleDateFormat fullDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");

    public CustomKey() {
        this.id = "";
        this.dateTime = "";
        this.channel = "";
        this.srchSi = "";
        this.hotelId = "";
        this.srchAdultsCount = "";
    }

    public CustomKey(String id, String  dateTime, String  channel, String  srchSi, String  hotelId, String srchAdultsCount) {
        this.id = id;
        this.dateTime = dateTime;
        this.channel = channel;
        this.srchSi = srchSi;
        this.hotelId = hotelId;
        this.srchAdultsCount = srchAdultsCount;
    }

    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        CustomKey.id = id;
    }

    public static String getDateTime() {
        return dateTime;
    }

    public static void setDateTime(String dateTime) {
        CustomKey.dateTime = dateTime;
    }

    public static String getChanel() {
        return channel;
    }

    public static void setChanel(String channel) {
        CustomKey.channel = channel;
    }

    public static String getSrchSi() {
        return srchSi;
    }

    public void setSrchSi(String srchSi) {
        try {
            this.srchSi = String.valueOf(fullDate.parse(srchSi).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getSrchAdultsCount() {
        return srchAdultsCount;
    }

    public String setSrchAdultsCount(String srchAdultsCount) {
        return srchAdultsCount;
    }

    public static String getHotelId() {
        return hotelId;
    }

    public static void setHotelId(String hotelId) {
        CustomKey.hotelId = hotelId;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(id);
        dataOutput.writeUTF(dateTime);
        dataOutput.writeUTF(channel);
        dataOutput.writeUTF(srchSi);
        dataOutput.writeUTF(hotelId);
        dataOutput.writeUTF(srchAdultsCount);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        id = dataInput.readUTF();
        dateTime = dataInput.readUTF();
        channel = dataInput.readUTF();
        srchSi = dataInput.readUTF();
        hotelId = dataInput.readUTF();
        srchAdultsCount = dataInput.readUTF();
    }

    @Override
    public int compareTo(CustomKey o) {
        if (o != null && o.getHotelId() != null && this.hotelId.compareTo(o.getHotelId()) != 0) {
            return this.hotelId.compareTo(o.getHotelId());
        } else if (o != null && o.getSrchSi() != null && this.srchSi.compareTo(o.getSrchSi()) != 0) {
            return this.srchSi.compareTo(o.getSrchSi());
        } else if (o != null && o.getId() != null && this.id.compareTo(o.getId()) != 0) {
            return this.id.compareTo(o.getId());
        } else {
            return 0;
        }

    }
    @Override
    public int hashCode() {
        return this.hotelId.hashCode();
    }

    @Override
    public String toString() {
        return "CustomKey {" +
                "hotelId = '" + hotelId + '\'' +
                ", srch_ci = '" + srchSi + '\'' +
                ", Id = '" + id + '\'' +
                ", channel = '" + channel + '\'' +
                ", dateTime = '" + dateTime + '\'' +
                ", Adults count = '" + srchAdultsCount + '\'' +
                '}';
    }

}
