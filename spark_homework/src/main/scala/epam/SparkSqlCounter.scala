package epam

import org.apache.spark.sql.{Dataset, Row}

import scala.util.Random

object SparkSqlCounter {
  private val spark = org.apache.spark.sql.SparkSession.builder
    .master("local")
    .appName("Spark Sql Counter")

    .getOrCreate;
  spark.sparkContext.setLogLevel("ERROR")

  def createSelect(str: String): Dataset[Row] = {
    spark.sql(str)
  }

  def createCsv(path: String): Dataset[Row] = {
    spark.read
      .format("csv")
      .option("header", "true")
      .load(path)
  }

  def main(args: Array[String]): Unit = {

    createCsv("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Master.csv")
      .createOrReplaceTempView("allPlayer")
    createCsv("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\AwardsPlayers.csv")
      .createOrReplaceTempView("allAwards")
    createCsv("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Goalies.csv")
      .createOrReplaceTempView("allGoals")
    createCsv("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Teams.csv")
      .createOrReplaceTempView(
        "allTeam"
      )
    createSelect("SELECT playerID as id, CONCAT(firstName ,' ', lastName) AS name FROM allPlayer")
      .createOrReplaceTempView("playerData")
    createSelect("Select playerID as id, COUNT(award) as award_cnt from allAwards " +
      "Group by id ")
      .createOrReplaceTempView("awards")
    createSelect("SELECT tmID as team_id, name FROM  allTeam")
      .createOrReplaceTempView("team")
    createSelect("SELECT tmID as teamId, playerID as id , SUM(PostGA) as goals FROM allGoals" +
      " Group by id, teamId ")
      .createOrReplaceTempView("goals")
    createSelect("SELECT award_cnt, awards.id, goals, teamId from awards left outer join goals where awards.id == goals.id")
      .createOrReplaceTempView("GoalsAwardJoin")
    createSelect("SELECT * FROM GoalsAwardJoin left join team where GoalsAwardJoin.teamId == team.team_id")
      .createOrReplaceTempView("teamGoalsAwardJoin")
    createSelect("select distinct playerData.name, award_cnt , goals , teamGoalsAwardJoin.name from teamGoalsAwardJoin " +
      "left join playerData where playerData.id == teamGoalsAwardJoin.id " +
      "order by goals desc " +
      "limit 10")
      .toJavaRDD.saveAsTextFile("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\result" + Random)

  }
}
