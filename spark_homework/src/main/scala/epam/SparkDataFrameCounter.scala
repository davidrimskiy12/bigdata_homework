package epam

import org.apache.spark.sql.{Dataset, Row}

import scala.util.Random

object SparkDataFrameCounter {

  private val spark = org.apache.spark.sql.SparkSession.builder
    .master("local")
    .appName("Spark Sql Counter")
    .getOrCreate;

  spark.sparkContext.setLogLevel("ERROR")

  private def createDataFrame(path: String): Dataset[Row] = {
    spark.read.format("csv")
      .option("header", "true")
      .load(path)
  }

  def createSelect(str: String): Dataset[Row] = {
    spark.sql(str)
  }

  def main(args: Array[String]): Unit = {
    val allPlayer = createDataFrame("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Master.csv")
      //.select(concat($"firstName" ,lit(" "), $"lastName"))
      .createOrReplaceTempView("allPlayer")

    val allAward = createDataFrame("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\AwardsPlayers.csv").toDF()
      .createOrReplaceTempView("allAwards")
    val allGoal = createDataFrame("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Goalies.csv").toDF()
      .createOrReplaceTempView("allGoals")
    val allTeam = createDataFrame("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Teams.csv").toDF()
      .select("tmID", "name")
      .createOrReplaceTempView("teams")

    createSelect("select playerID, concat(firstName,' ',lastName) as Name from allPlayer").toDF()
      .createOrReplaceTempView("playerData")
    createSelect("Select playerID as id, COUNT(award) as award_cnt from allAwards " +
      "Group by id ").createOrReplaceTempView("awards")
    createSelect("SELECT tmID as teamId, playerID as id , SUM(PostGA) as goals FROM allGoals" +
      " Group by id, teamId ").createOrReplaceTempView("goals")
    val finalSelect = createSelect("SELECT distinct playerData.Name , award_cnt, goals, teams.name from awards " +
      "left join goals on awards.id == goals.id " +
      "left join playerData on playerData.playerID == awards.id " +
      "left join teams on teams.tmID == goals.teamId " +
      "order by goals desc " +
      "limit 10")
    finalSelect.toJavaRDD.saveAsTextFile("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\result" + Random)


  }
}
