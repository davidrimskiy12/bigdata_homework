package epam

import java.io.{File, PrintWriter}
import java.nio.file.Paths

import javassist.bytecode.StackMapTable.Writer
import org.apache.spark.api.java.JavaPairRDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.JavaConverters._
import scala.util.Random

object SparkCoreCounter {
  def parse(a: String): Double = {
    if (a != null && a.length > 0 && !a.equals("PostGA") ) {
      a.toDouble
    }
    else {
      0
    }
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("Sum Goals").setMaster("local[*]")

    val sc = new SparkContext(conf)
    val allGoals = sc.textFile("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Goalies.csv") //spark.read.format("csv").option("header","true").load("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Goalies.csv")
    val allAwards = sc.textFile("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\AwardsPlayers.csv")
    val allPlayers = sc.textFile("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Master.csv")
    val allTeam = sc.textFile("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Teams.csv")

    val goals: JavaPairRDD[String, Double] = allGoals.toJavaRDD().mapToPair(line => {
      val data = line.split(",", -1)
      Tuple2[String, Double](data(0), parse(data(21)))
    }).reduceByKey(_ + _)
    // .groupByKey().mapToPair(x => Tuple2(x._1, sumA(x._2.asScala)))

    val goalsSort: JavaPairRDD[Double, String] = goals.mapToPair(x =>Tuple2 (x._2, x._1)).sortByKey(false)
    val goalsLast :JavaPairRDD[String,Double] = goalsSort.mapToPair(x=>Tuple2(x._2,x._1))

    val playerId: JavaPairRDD[String, String] = allPlayers.toJavaRDD().mapToPair(line => {
      val values: Array[String] = line.split(",")
      var playerId = ""
      for (id: String <- Array(values(0), values(1), values(2))) {
        if (id != "") {
          playerId = id
        }
        if (playerId == "") playerId = "unknown"
      }
      new Tuple2[String, String](playerId, String.join(" ", values(3), values(4)))
    })

    val awards: JavaPairRDD[String, Double] = allAwards.toJavaRDD().mapToPair(line => {
      val values = line.split(",")
      new Tuple2[String, Double](values(0), 1)
    })
    val teams: JavaPairRDD[String, String] = allTeam.toJavaRDD().mapToPair(line => {
      val values = line.split(",",-1)
      new Tuple2[String, String](values(2), values(18))
    })
    val awardPlayerJoin: JavaPairRDD[String, Tuple2[String, Double]] = playerId.join(awards)
    val awardPlayerJoinWithouId: JavaPairRDD[String, Double] = awardPlayerJoin.mapToPair(x => Tuple2(x._1, x._2._2))
    val playerIdAwardSort :JavaPairRDD[String,Double]= awardPlayerJoinWithouId.reduceByKey(_ + _).sortByKey()
    val goalsForJoin: JavaPairRDD[String, String] = allGoals.toJavaRDD().mapToPair(line => {
      val values = line.split(",", -1)
      new Tuple2[String, String](values(3), values(0))
    })

    val teamJoin : JavaPairRDD[String,Tuple2[String,String]] = goalsForJoin.join(teams)
    val teamWithout :JavaPairRDD[String,String] = teamJoin.mapToPair(x=>Tuple2(x._2._1,x._2._2)).distinct()
    val join = playerIdAwardSort.join(teamWithout).groupByKey()
    val fullJoin = join.join(goalsLast).keyBy(x=>x._2._2).sortByKey(false).map(x=>(x._2._1,x._2._2,x._1)).take(10)
    val writer = new PrintWriter(new File("D:\\java\\projects\\scala\\untitled\\src\\main\\files\\Resulre.txt"))
  //top 10, but in 1 line, when used .take(10)
    writer.write(fullJoin.toString)
    writer.close()
    //normall view, but all players
//   fullJoin
//      .saveAsTextFile("D:\\\\java\\\\projects\\\\scala\\\\untitled\\\\src\\\\main\\\\files\\\\result" + Random)

  }
}
