package epam


import org.apache.spark.sql.streaming.{GroupState, GroupStateTimeout, OutputMode}
import org.apache.spark.sql.{SparkSession, functions}

import scala.collection.mutable.ListBuffer

case class UserSessionState(var user: String,
                            var activity: String,
                            var start: java.sql.Timestamp,
                            var end: java.sql.Timestamp)

case class UserSessionInfo(user: String, activity: String, start: java.sql.Timestamp, end: java.sql.Timestamp)

case class UserActivity(user: String, activity: String, start: java.sql.Timestamp)

object Consumer {
  def updateAcrossAllUserActivities(user: String,
                                    inputs: Iterator[UserActivity],
                                    oldState: GroupState[UserSessionState]):
  Iterator[UserSessionInfo] = {
    var output = ListBuffer[UserSessionInfo]()
    val sorted = inputs.toList.sortBy(_.start.getTime)
    if (oldState.exists) {
      val state = oldState.get
      output += UserSessionInfo(state.user, state.activity, state.start, sorted.head.start)
    }
    for (i <- 0 to sorted.length - 2) {
      output += UserSessionInfo(sorted(i).user, sorted(i).activity, sorted(i).start, sorted(i + 1).start)
    }
    val last = sorted.last
    output += UserSessionInfo(last.user, last.activity, last.start, null)
    oldState.update(UserSessionState(last.user, last.activity, last.start, null))
    output.iterator
  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("Streaming")
      .master("local")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    val frame = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .option("subscribe", "Activities")
      .load()
    import spark.implicits._
    val f = frame
      .selectExpr("CAST (value as string) as value")
      .selectExpr(
        "split(value, ',')[0] as user",
        "split(value, ',')[1] as activity",
        "cast(split(value, ',')[2] as timestamp) as start"
      )
    val activityDS = f.as[UserActivity]
    activityDS.withWatermark("start", "1 minutes")
      .groupByKey(_.user)
      .flatMapGroupsWithState(OutputMode.Update, GroupStateTimeout.EventTimeTimeout)(updateAcrossAllUserActivities)
      .writeStream
      .outputMode("update")
      .format("console")
      .option("truncate", false)
      .start()
    activityDS.groupBy(functions.window($"start", "5 minutes", "5 minutes"), $"user")
      .count()
      .orderBy("window")
      .writeStream
      .outputMode("complete")
      .format("console")
      .option("truncate", false)
      .start().awaitTermination()
    spark.stop()
  }
}


