package epam

import java.sql.Timestamp
import java.util.{Properties, Scanner}

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.StringSerializer

object Producer {
  val TOPIC_NAME = "Activities"
  val in = new Scanner(System.in)

  def main(args: Array[String]): Unit = {
    val pam = org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG
    print(pam)
    val kafkaProps = new Properties
    kafkaProps.put("bootstrap.servers", "localhost:9092")
    kafkaProps.put("key.serializer", classOf[StringSerializer].getName)
    kafkaProps.put("value.serializer", classOf[StringSerializer].getName)
    val producer = new KafkaProducer[String, String](kafkaProps)
    while ( {
      true
    }) {
      System.out.println("Enter name:")
      val name = in.nextLine
      System.out.println("Enter activity:")
      val activity = in.nextLine
      val timestamp = new Timestamp(System.currentTimeMillis)
      val record = new ProducerRecord[String, String](TOPIC_NAME, name, name + "," + activity + "," + timestamp)
      try producer.send(record)
      catch {
        case e: Exception =>
          e.printStackTrace()
      }
    }
  }
}
